from typing import Optional

from dataclasses import dataclass
from dataclasses_json import DataClassJsonMixin, dataclass_json

def get_instance():
    class SimpleClass:
        name: str
        snake_case_field: int
    
    class SimpleClassWithInit:
        name: str
        snake_case_field: int
        
        def __init__(self, name: str, snake_case_field: int):
            self.name = name
            self.snake_case_field = snake_case_field
    
    @dataclass
    class Dataclass:
        name: str
        snake_case_field: int
    
    @dataclass
    class InheritedJsonDataclass(DataClassJsonMixin):
        name: str
        snake_case_field: int
    
    @dataclass_json
    @dataclass
    class DecoratedJsonDataclass:
        name: str
        snake_case_field: int
    
    @dataclass_json
    @dataclass
    class JsonDataClassWithOptionals:
        name: Optional[str] = None
        snake_case_field: Optional[int] = None
    
    NAME = "Mark"
    FIELD = 15
    
    valid_json_cc = { 'name': NAME, 'snakeCaseField': FIELD }
    valid_json_sc = { 'name': NAME, 'snake_case_field': FIELD }
    only_name_json = { 'name': NAME }
    
    simple_class_instance = SimpleClass()
    simple_class_instance.name = NAME
    simple_class_instance.snake_case_field = FIELD
    
    simple_class_with_init_instance = SimpleClassWithInit(NAME, FIELD)
    dataclass_instance = Dataclass(NAME, FIELD)
    inherited_json_dataclass_instance = InheritedJsonDataclass(NAME, FIELD)
    decorated_json_dataclass_instance = DecoratedJsonDataclass(NAME, FIELD)
    empty_optionals_dataclass = JsonDataClassWithOptionals()
    only_name_optionals_dataclass = JsonDataClassWithOptionals(NAME)
    
    l = locals()
    _qualname = f'{get_instance.__qualname__}.<locals>.'
    for name, cls in list(l.items()):
        if (isinstance(cls, type)):
            _, _, cls.__qualname__ = cls.__qualname__.rpartition(_qualname)
    
    return l
